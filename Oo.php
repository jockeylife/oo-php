<?php 

class Oo{

public function __construct() {
        //echo "<p>". __CLASS__ ." Class construct </p>";
    }

    function __destruct() {
      // echo "<p>".__CLASS__ . " Class destruct </p>";
    }

public function arrayData($type){
echo " ".__FUNCTION__.": ";
    if ($type=="ind")
    $array = array ("skills","items","tenure");
    
    if ($type=="assoc")    
    $array = array("sex"=>"male", "height"=>"5.8");
    
    if ($type=="multi")
    $array = array(
                         array(
                          "D" => "Sr. Associate � Technology",
                          "C" => "Magnon Solutions Pvt. Ltd.",
                          "L" => "Worli",
                          "B" => "Apr 11",
                          "E" => "May 12",
                         
                         ) ,
                      );
                      
    return $array;
}

public function checkSwitch($value){
echo " ".__FUNCTION__.": ";
    $array = $this->arrayData("ind");
switch ($value) {
    case 0:
        echo $array["$value"]." ";
        break;
    case 1:
        echo $array["$value"]." ";
        break;
    case 2:
        echo $array["$value"]." ";
        break;
    default:
       echo "!= 0, 1 or 2";
}
}

public function onSwitch($array=array()){
echo " ".__FUNCTION__.": ";
 foreach($array as $key=>$arr){
 $this->checkSwitch($key);   
 }
}

public function hello(){
echo " ".__FUNCTION__.": ";
echo " OO Class ";
}

public function arrayPrint($array,$type){
echo " ".__FUNCTION__.": ";
if ($type=="ind"){
foreach($array as $key=>$arr){
echo "<p>";
echo "Key No: ".$key." and Value: ". $arr;
echo "</p>";
 }
}

if ($type=="assoc"){
foreach($array as $key=>$arr){
echo "<p>";
echo "Key No: ".$key." and Value: ". $arr;
echo "</p>";
 }
}

if ($type=="multi")
{
echo '<table width=100%><tr><th>Sr No.</th><th>D</th><th>C</th><th>L</th><th>B</th><th>E</th><th>W</th> </tr>';
        foreach($array as $key => $value) {
            echo "<tr> <th>$key</th>";
            foreach($value as $data) {
                echo "<th>$data</th>";
            }
            echo "</tr>";
        }
        echo '</table>';
}
}

public function testRecursive(){

    static $count = 0;
    if ($count==0)
echo " ".__FUNCTION__.": ";
    $count++;
    echo $count;
    if ($count < 3) {
        $this->testRecursive();
       echo $count;
    }
    $count--;
}

public function globalSum(){

echo " ".__FUNCTION__.": ";

    global $a, $b;
 
    $b = $a + $b;
 
    $GLOBALS['c'] = $GLOBALS['a'] + $GLOBALS['b'];
   
    return $b." ".$GLOBALS['c'];

}

public function create_auto_array_num($len){
    $n=array("0","1","2","3","4","5","6","7","8","9");
    for($i=0; $i<=$len; $i++){     
    $array[] = array($i=>$i);    
    }
    return $array;
    }
    
public function create_auto_array_alfa_num($len,$nlen){
    $n=array("0","1","2","3","4","5","6","7","8","9");
    $a=array ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
    for($i=0; $i<=$len; $i++){     
    
    for($o=0; $o<$nlen; $o++){
      $array[] = array ($a[$i]=>$o);
        
    }
    }
    return $array;
    }
    
       public function create_auto_array_alfa_num_alfa($len,$nlen){
    $n=array("0","1","2","3","4","5","6","7","8","9");
    $a=array ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
    
    for($i=0; $i<=$len; $i++){     
    
    for($o=0; $o<$nlen; $o++){
    
           $array[] = array ($a[$i]=>array($a[$o]=>$o));
      
    }
    }
    return $array;
    }

    
    
public function create_auto_array_alfa($len){
    $n=array("0","1","2","3","4","5","6","7","8","9");
        $a=array ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
        for($i=0; $i<=$len; $i++){ 
        $array[] = array($a[$i]);
    }
      return $array;
    }
    
    public function test_and_expression($array,$array_alfa_num_alfa){


    
for($n=0; $n<=count($array); $n++){

foreach ($array[$n] as $key=> $nuu){

$a = $b = $c = $nuu;
 
if ($a==0){
$a= $b = $nuu; 
$c = $nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

$b = $nuu; 
$c = 1+$nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

}

if ($a==1){
$a= $nuu;
$b = $nuu; 
$c = $nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

$a= $nuu;
$b = 1-$nuu; 
$c = 1-$nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

$a = 1-$nuu; 
$b = $nuu;
$c = 1-$nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

$a = $nuu; 
$b = $nuu;
$c = 1-$nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

$a = $nuu; 
$b = 1-$nuu;
$c = $nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

$a = 1-$nuu; 
$b = $nuu;
$c = $nuu; 
$d = (($a && $b || $c) || ($a AND $b || $c));
echo " A: ".$a." B: ".$b." C: ".$c." D: ".$d." <br/>";

}
}
}
    }

}

?>